﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTile : GameTile
{
    public override void Initialize(GamePosition pos)
    {
        Blocked = false;
        base.Initialize(pos);
    }
}
