﻿using UnityEngine;

public class GamePosition
{
    public int Row
    {
        private set; get;
    }

    public int Col
    {
        private set; get;
    }

    public GamePosition(int col, int row)
    {
        Row = row;
        Col = col;
    }

    public static bool operator == (GamePosition gp1, GamePosition gp2)
    {
        if (gp1.Row == gp2.Row && gp1.Col == gp2.Col)
            return true;

        return false;
    }

    public static bool operator != (GamePosition gp1, GamePosition gp2)
    {
        if (gp1.Row != gp2.Row || gp1.Col != gp2.Col)
            return true;

        return false;
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}

public class GameTile : MonoBehaviour {
    public int Row;
    public int Col;
    public GamePosition GamePosition;

    public bool Blocked
    {
        protected set; get;
    }

    public virtual void Initialize(GamePosition pos)
    {
        GamePosition = pos;
        Row = GamePosition.Row;
        Col = GamePosition.Col;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
