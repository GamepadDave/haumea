﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class PathFinding : MonoBehaviour
{
    private List<GameTile> finalPath;
    public GameTile[,] gameTiles;
    private bool[,] closed;
    private float[,] cost;
    private GamePosition[,] link;
    private bool[,] inPath;

    // Should retrieve from some global value
    private GamePosition levelSize = new GamePosition(50,50);

    void Start()
    {
        finalPath = new List<GameTile>();
        closed = new bool[levelSize.Col, levelSize.Row];
        cost = new float[levelSize.Col, levelSize.Row];
        link = new GamePosition[levelSize.Col, levelSize.Row];
        inPath = new bool[levelSize.Col, levelSize.Row];

        for (int i = 0; i < levelSize.Col; i++)
        {
            for (int j = 0; j < levelSize.Row; j++)
            {
                closed[i, j] = false; //Set all positions open
                cost[i, j] = 999999; //Set an unachievably large pair cost
                link[i, j] = new GamePosition(-1, -1);
                inPath[i, j] = false;
            }
        }
    }

    public List<GameTile> Build(GameTile StartTile, GameTile DestinationTile)
    {
        gameTiles = new GameTile[levelSize.Col, levelSize.Row];
        // Build local reference to game tiles
        var tiles = FindObjectsOfType<GameTile>();
        foreach (GameTile t in tiles)
        {
            gameTiles[t.GamePosition.Col, t.GamePosition.Row] = t;
        }

        GamePosition startPoint = StartTile.GamePosition;
        GamePosition destination = DestinationTile.GamePosition;
        finalPath.Clear();

        for (int col = 0; col < levelSize.Col; ++col)
        {
            for (int row = 0; row < levelSize.Row; ++row)
            {
                closed[col, row] = false; //Set all positions open
                cost[col, row] = 999999; //Set a high cost
                link[col, row] = new GamePosition(-1, -1);
                inPath[col, row] = false;
            }
        }

        closed[startPoint.Col, startPoint.Row] = false;
        cost[startPoint.Col, startPoint.Row] = 0;

        while (!closed[destination.Col, destination.Row])
        {
            float lowestCost = 999999;
            GamePosition lowestCostLoc = new GamePosition(-1, -1);

            for (int col = 0; col < levelSize.Col; ++col)
            {
                for (int row = 0; row < levelSize.Row; ++row)
                {
                    float heuristic = Mathf.Abs(col - destination.Col) + Mathf.Abs(row - destination.Row); //A*  // manhatthan

                    //Check to see if there is a lower cost available, if so, save that position
                    if ((cost[col, row] + heuristic) <= lowestCost && !closed[col, row])
                    {
                        lowestCost = cost[col,row];
                        lowestCostLoc = new GamePosition(col, row);
                    }
                }
            }

            closed[lowestCostLoc.Col, lowestCostLoc.Row] = true;

            //Calculate the new cost for neighbouring blocks
            for (int nCol = -1; nCol <= 1; nCol++)
            {
                for (int nRow = -1; nRow <= 1; nRow++)
                {
                    if (ValidPosition(new GamePosition(lowestCostLoc.Col + nCol, lowestCostLoc.Row + nRow))) //Check the neighbour is not out of bounds
                    {
                        float newCost = 999999;

                        if (nCol != 0 ^ nRow != 0) //Adjacent
                            newCost = lowestCost + 1;
                        //else if (nCol != 0 && nRow != 0) //Diagonal
                          //  newCost = lowestCost + 1.4f;

                        //If the newly calculated cost is lower, and it is not blocked by a wall
                        if (newCost < cost[lowestCostLoc.Col + nCol, lowestCostLoc.Row + nRow]
                            && ValidPosition(new GamePosition(lowestCostLoc.Col + nCol, lowestCostLoc.Row + nRow)))
                        {
                            cost[lowestCostLoc.Col + nCol, lowestCostLoc.Row + nRow] = newCost; //Assign its new cost
                            link[lowestCostLoc.Col + nCol, lowestCostLoc.Row + nRow] = new GamePosition(lowestCostLoc.Col, lowestCostLoc.Row);
                        }
                    }
                }
            }
        }// while loop terminated

        bool done = false;
        GamePosition nextClosed = destination; //start of path
        while (!done)
        {
            if (nextClosed == new GamePosition(-1, -1))
            {
                return null;
            }
            finalPath.Add(gameTiles[nextClosed.Col, nextClosed.Row]);
            inPath[nextClosed.Col, nextClosed.Row] = true;
            nextClosed = link[nextClosed.Col, nextClosed.Row];
            if (nextClosed == startPoint)
            {
                done = true;
            }
        }

        finalPath.Reverse();

        return finalPath;
    }

    private bool ValidPosition(GamePosition pos)
    {
        if (pos.Col < 0 || pos.Row < 0) return false;
        if (pos.Col >= levelSize.Col || pos.Row >= levelSize.Row) return false;
        if (gameTiles[pos.Col, pos.Row].Blocked) return false;

        return true;
    }
}
