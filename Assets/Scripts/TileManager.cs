﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {
    public GameObject FloorTilePrefab;
    public GameObject WallTilePrefab;

    GamePosition GameSize = new GamePosition(50, 50);
	// Use this for initialization
	void Start () {
        System.Random rnd = new System.Random();
        for (int col = 0; col < GameSize.Col; col++)
        {
            for (int row = 0; row < GameSize.Row; row++)
            {
                GameObject ToInstantiate = FloorTilePrefab;
                Vector3 ToInstantiatePos = Vector3.zero;
                int wall = rnd.Next(0,10);
                if (wall <= 2 && row != 0 && col != 0 && row != GameSize.Row - 1 && col != GameSize.Col - 1)
                {
                    ToInstantiatePos = new Vector3(0, 0.225f, 0);
                    ToInstantiate = WallTilePrefab;
                }
                GameObject obj = Instantiate(ToInstantiate, ToInstantiatePos + new Vector3(col, 0, -row), Quaternion.identity);
                GameTile gt = obj.GetComponent<GameTile>();
                gt.Initialize(new GamePosition(col, row));
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(1))
        {
            FindObjectOfType<Worker>().BuldPath();
            //Debug.Log("hello");
        }
	}
}
