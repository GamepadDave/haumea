﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTile : GameTile {

    public override void Initialize(GamePosition pos)
    {
        Blocked = true;
        base.Initialize(pos);
    }
}
