﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour {
    PathFinding path;
    public GameTile currentTile;
    public GameTile destinationTile;

    public GameTile MoveTowardTarget = null;
    public Vector3 MoveTowardTargetPosition = Vector3.zero;
    public List<GameTile> tilePath;
    public float speed = 1;
	// Use this for initialization
	void Start () {
        path = GetComponent<PathFinding>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (MoveTowardTarget != null)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, MoveTowardTargetPosition, step);

            if (Vector3.Distance(transform.position, MoveTowardTargetPosition) <= 0.01f)
            {
                MoveTowardTarget = tilePath.Count > 0 ? path.gameTiles[tilePath[0].Col, tilePath[0].Row] : null;
                if (MoveTowardTarget != null)
                {
                    currentTile = path.gameTiles[tilePath[0].Col, tilePath[0].Row];
                    MoveTowardTargetPosition = GetTargetPosition(MoveTowardTarget);
                    tilePath.RemoveAt(0);
                }
            }
        }
	}

    public void BuldPath()
    {
        tilePath = path.Build(currentTile, destinationTile);
        if (tilePath == null)
        {
            Debug.LogError("Cannot Find Path");
            return;
        }
        MoveTowardTarget = path.gameTiles[tilePath[0].Col, tilePath[0].Row];
        MoveTowardTargetPosition = new Vector3(MoveTowardTarget.transform.position.x, transform.position.y, MoveTowardTarget.transform.position.z);
        tilePath.RemoveAt(0);
    }

    private Vector3 GetTargetPosition(GameTile gt)
    {
        return new Vector3(gt.transform.position.x, transform.position.y, gt.transform.position.z);
    }
}
